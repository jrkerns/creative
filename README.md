# GitLab Pages - Branched

There's just this file in the `master` branch and the pages live in the, you guessed, `pages` branch.

View project: https://gitlab.com/jekyll-themes/creative/tree/pages

View site: https://jekyll-themes.gitlab.io/creative/

----

_Forked from https://gitlab.com/steko/test/_